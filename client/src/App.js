import React, { Component } from 'react';
import './App.css';

class App extends Component {
  state = { users: [] }

  componentDidMount() {
    fetch('users')
      .then(res => res.json())
      .then(users => this.setState({ users }));
  }

  render() {
    return (
      <div className="App">
        <h1>Utility Dashboard</h1>
        <ul>
          {this.state.users.map(user => 
            <li key={user}>
              {user.year}|{user.month}|{user.kwh}|{user.bill}|${user.savings}
            </li>
          )}
        </ul>
      </div>
    );
  }
}

export default App;
